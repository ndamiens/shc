shc (4.0.3-1) unstable; urgency=medium

  * Maintainer upload
  * Merge the NMU from the NMU-master branch
    * New upstream version 4.0.3
    * debian/control:
      - Added 'Rules-Requires-Root: no' to debian/control source stanza.
      - Bumped Standards-Version to 4.4.1.
      - Updated upstream address in Homepage field.
    * debian/copyright:
      - Added a comment in header to quote the original upstream homepage.
      - Added rights for all upstreams.
      - Added Upstream-Contact field.
      - Added Eriberto as packager.
    * debian/patches/010_fix-rc-path.patch: created to fix 'rc' path to allow
      the build system run tests.
    * debian/salsa-ci.yml: created to provide CI tests via Salsa.
    * debian/tests/*: created to provide some trivial CI tests.
    * debian/watch: remade to look at releases instead of tags.
  * fix package-uses-old-debhelper-compat-version 12
  * fix "source: out-of-date-standards-version" problem
  * update copyright year for packager
  * remove broken tests that failed since day one

 -- Tong Sun <suntong001@users.sourceforge.net>  Sun, 08 Nov 2020 15:42:56 +0100

shc (4.0.3-0.1) unstable; urgency=medium

  * Non-maintainer upload. (Closes: #942544)
  * New upstream version 4.0.3
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control:
      - Added ash, ksh, rc, tcsh, zsh to Build-Depends field to allow 'make
        test' command work fine.
      - Added 'Rules-Requires-Root: no' to debian/control source stanza.
      - Bumped Standards-Version to 4.4.1.
      - Updated upstream address in Homepage field.
      - Updated VCS fields to use Salsa.
  * debian/copyright:
      - Added a comment in header to quote the original upstream homepage.
      - Added rights for all upstreams.
      - Added Upstream-Contact field.
      - Fixed rights for packagers.
  * debian/rules:
      - Enabled DEB_BUILD_MAINT_OPTIONS variable to provide a full hardening to
        final binary.
      - Removed all trash.
  * debian/patches/010_fix-rc-path.patch: created to fix 'rc' path to allow the
    build system run tests.
  * debian/salsa-ci.yml: created to provide CI tests via Salsa.
  * debian/tests/*: created to provide some trivial CI tests.
  * debian/upstream/signing-key.asc: removed because the upstream didn't sign
    the tarballs since 3.9.6 version. Currently, it can result in a rejection
    when uploading the package.
  * debian/watch: remade to look at releases instead of tags.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 17 Oct 2019 13:47:33 -0300

shc (4.0.2-1) UNRELEASED; urgency=medium

  * New upstream version 4.0.2
  * - [!] fix all reported package issues with: new DH level, new
      Standards-Version, changes in description, updates rules, copyright etc
      in debian
  * - [!] fix copyright, deprecated d/compat, and warning of
      testsuite-autopkgtest-missing, hardening-no-bindnow. Thanks to
      Eriberto <eriberto@eriberto.pro.br>
  * - [+] add DEP-12 upstream metadata
  * - [!] fix Vcs address

 -- Tong Sun <suntong001@users.sourceforge.net>  Sat, 06 Jul 2019 12:17:45 -0400

shc (4.0.1+git20190127.699738e-1) UNRELEASED; urgency=medium

  * New upstream release (4.0.1).
  * - [!] fix copyright & home url
  * - [-] remove contributors from the Debian copyright file
  * - [!] fix watch file download filename

 -- Tong Sun <suntong001@users.sourceforge.net>  Sat, 27 Apr 2019 19:40:46 +0000

shc (3.9.6-1) unstable; urgency=medium

  * Fix for infinite loop (Closes: #861180)
  * Fix issue #36 (at https://github.com/neurobin/shc/issues/36)
  * Fix issue #38 (at https://github.com/neurobin/shc/issues/38)

 -- Tong Sun <suntong001@users.sourceforge.net>  Sun, 08 Jul 2018 15:11:21 -0400

shc (3.8.9b-1) unstable; urgency=low

  * new upstream release (3.8.9b)
  * bump up Standards-Version to 3.9.6
  * remove change-Makefile-for-Debian.diff patch
  * add change-makefile-for-Debian.diff patch
  * drop debian/README.source as the 3.8.9 bug has been fixed

 -- Tong Sun <suntong001@users.sourceforge.net>  Wed, 16 Sep 2015 03:22:50 +0000

shc (3.8.7-2) unstable; urgency=medium

  * Fix ptraceable directive so that it can work out of box under Ubuntu

 -- Tong Sun <suntong001@users.sourceforge.net>  Sun, 07 Sep 2014 03:15:49 +0000

shc (3.8.7-1) unstable; urgency=low

  * Initial Debian Release (Closes: #735946)

 -- Tong Sun <suntong001@users.sourceforge.net>  Tue, 14 Jan 2014 17:25:58 -0500
